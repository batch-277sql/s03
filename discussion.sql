-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values)


INSERT into artists(name) VALUES ("Rivermaya");


INSERT into artists(name) VALUES ("J.cole");

INSERT into artists(name) VALUES ("Eminem");


INSERT into artists(name) VALUES ("Kendrick Lamar");
INSERT into artists(name) VALUES ("Loonie");




INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Forest Hills Drive", "2014-01-01" , 2);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Encore", "2016-01-01" , 3);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Off Season", "2020-01-01" , 2);



INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The Classics", "2008-01-01" , 3);



INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Section 80", "2014-01-01" , 4);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("DAMN", "2016-01-01" , 4),
	("Tao Lang", "2012-01-01", 5)
,






INSERT INTO songs(song_name, genre, length, album_id) VALUES ("NO Role Modelz", "Hiphop", 190, 2),
	("Not Afraid", "Hiphop", 220, 3)


INSERT INTO songs(song_name, genre, length, album_id) VALUES ("Bitch Don't Kill my Vibe", "Hiphop", 320, 6);


INSERT INTO songs(song_name, genre, length, album_id) VALUES ("Bitch Don't Kill my Vibe", "Hiphop", 320, 6);




INSERT INTO songs(song_name, genre, length, album_id) VALUES ("Love Yourz", "Hiphop", 240, 1);





INSERT INTO songs(song_name, genre, length, album_id) VALUES ("Spend Some time", "Hiphop", 440, 2);



INSERT INTO songs(song_name, genre, length, album_id) VALUES ("Pride is the Devil", "Hiphop", 330, 3),
	("Amari", "Hiphop", 250, 3);


INSERT INTO songs(song_name, genre, length, album_id) VALUES ("We all Die One Day", "Hiphop", 252, 4),
	("Say What You Say", "Hiphop", 150, 4);






-- [SECTION] READ DATa from database
-- SYNTAX :
 	-- SELECT <column_names> FROM <table_name>


 SELECT * FROM songs

 -- SPECIFY COLUMNS

 SELECT song_name FROM songs;



 SELECT song_name, genre FROM songs;



 -- with condition filter

 SELECT * FROM songs WHERE album_id = 6;


 -- WE can use AND and OR for multiple expressions


 -- Display title and length of the songs the are more than 4 mins


 SELECT song_name, length FROM songs WHERE length > 400 AND genre = "Hiphop";


 -- [SECTION] UPDATE RECORDS/DATA
 -- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condition>



UPDATE songs SET song_name = "Bitch Don't Kill My Vibe" WHERE id = 4;


-- TO target NULL .... Where genre is NULL


-- [SECTION] DELETING RECORDS
-- DELETE FROM <table_name> WHERE <condition>

-- Delete songs


DELETE FROM songs WHERE genre = "Hiphop" AND length < 200;




-- Mini activity
		-- Delete songs that are greater than 5 minutes

DELETE FROM songs WHERE genre = "Hiphop" AND length > 500;





